from util import get_input

def find_subset(iterable: list, target: int, r: int=2) -> list:
    '''Find all subsets of iterable whose sum == target and which contain r number of items (default 2).'''
    import itertools
    return list(filter(lambda subset: sum(subset) == target, itertools.combinations(iterable, r)))

def solve(iterable, depth: int=2):
    subset = find_subset(iterable, 2020, depth)
    import math
    return math.prod(subset[0])

if __name__ == '__main__':
    import sys, os
    expense_report = [int(i) for i in get_input(1)]
    print(f'The solution to the first part is: {solve(expense_report)}.')
    print(f'The solution to the second part is: {solve(expense_report, 3)}.')
