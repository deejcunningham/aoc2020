from util import get_input

def _extract_pw_parts(line: str):
    parts = line.split()
    letter_range = parts[0].split('-')
    char_min = int(letter_range[0])
    char_max = int(letter_range[1])
    char = parts[1].strip(':')
    password = parts[-1]
    return {
        'min': char_min,
        'max': char_max,
        'char': char,
        'password': password,
    }

def _check_char_count(line: str) -> bool:
    pw = _extract_pw_parts(line)
    return pw.get('min') <= pw.get('password').count(pw.get('char')) <= pw.get('max')

def solve_part_1(passwords: list):
    return len(list(filter(_check_char_count, passwords)))

def _check_char_position(line: str) -> bool:
    pw = _extract_pw_parts(line)
    # get the characters at the two positions
    subset = [pw.get('password')[pw.get('min')-1], pw.get('password')[pw.get('max')-1]]
    # exactly one of them should be the character in question
    return subset.count(pw.get('char')) == 1

def solve_part_2(passwords: list):
    return len(list(filter(_check_char_position, passwords)))

if __name__ == '__main__':
    import sys, os
    passwords = get_input(2)
    print(f'The solution to the first part is: {solve_part_1(passwords)}.')
    print(f'The solution to the second part is: {solve_part_2(passwords)}.')
