from util import get_input

class TreeMapPosition:
    def __init__(self, x, y, is_tree=None):
        self.x = x
        self.y = y
        self.is_tree = is_tree

class TreeMap:
    def __init__(self, raw_tree_map: list):
        self.grid = raw_tree_map
        self.begin = TreeMapPosition(0,0)
        self.width = len(self.grid[0])
        self.height = len(self.grid)

    def _get_raw_coords(self, slope: set) -> list:
        # get multiples of x and y until y is > self.height
        return zip(range(0, slope[0] * self.height, slope[0]), range(0, self.height, slope[1]))

    def _map_point(self, point: set) -> TreeMapPosition:
        # Take a raw coordinate and return the equivalent wrapped TreeMapPosition
        x = point[0]
        wrapped_x = (x % self.width) if x > self.width - 1 else x  # remainder after modulo self.width?
        y = point[1]
        return TreeMapPosition(
            x = wrapped_x,
            y = y,
            is_tree = self.grid[y][wrapped_x] == '#'
        )

    def _get_positions(self, slope: set):
        # Get all TreeMapPositions along slope
        raw_coords = self._get_raw_coords(slope)
        return map(self._map_point, raw_coords)

    def get_tree_count(self, slope: set):
        return len(list(filter(lambda pos: pos.is_tree, self._get_positions(slope))))

####

def solve_part_1(tree_map: list, slope: set) -> int:
    return TreeMap(tree_map).get_tree_count(slope)

def solve_part_2(tree_map: list) -> int:
    tree_map = TreeMap(tree_map)
    slopes = [
        (1,1),
        (3,1),
        (5,1),
        (7,1),
        (1,2),
    ]
    import math
    return math.prod([tree_map.get_tree_count(slope) for slope in slopes])

if __name__ == '__main__':
    import sys, os
    tree_map = get_input(3)
    print(f'The solution to the first part is: {solve_part_1(tree_map, (3,1))}.')
    print(f'The solution to the second part is: {solve_part_2(tree_map)}.')
