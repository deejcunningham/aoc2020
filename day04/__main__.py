from util import get_input

def get_passports(raw_input: list) -> list:
    # slice off list chunk up to the newline element: ''
    passports = []
    while len(raw_input) > 0:
        try:
            stop = raw_input.index('')
        except ValueError:  # index() will throw this on the last chunk b/c no '' exists
            stop = len(raw_input)
        finally:
            next_chunk = []
            for i in raw_input[:stop]:
                next_chunk.extend(i.split())
            # next_chunk = [i.split() for i in raw_input[:stop]]  # items in a chunk can actually be groups of items separated by ' ', so break those into individual items here
            passports.append(munge_input_chunk(next_chunk))
            raw_input = raw_input[stop+1:]  # we've processed this chunk, so drop it

    return passports

def munge_input_chunk(raw_passport: list) -> dict:
    # take a list of "key:value" strings and convert it into a dict
    passport = {}
    for item in raw_passport:
        key, value = item.split(':')
        passport[key] = value
    return passport

def _validate_byr(byr: str) -> bool:
    return len(byr) == 4 and 1920 <= int(byr) <= 2002

def _validate_iyr(iyr: str) -> bool:
    return len(iyr) == 4 and 2010 <= int(iyr) <= 2020

def _validate_eyr(eyr: str) -> bool:
    return len(eyr) == 4 and 2020 <= int(eyr) <= 2030

def _validate_hgt(hgt: str) -> bool:
    if hgt.endswith('cm'):
        return 150 <= int(hgt[:-2]) <= 193
    elif hgt.endswith('in'):
        return 59 <= int(hgt[:-2]) <= 76
    return False

def _validate_hcl(hcl: str) -> bool:
    return hcl.startswith('#') and len(hcl) == 7 and hcl[1:].isalnum()

def _validate_ecl(ecl: str) -> bool:
    return ecl in ['amb', 'blu', 'brn', 'gry', 'grn', 'hzl', 'oth']

def _validate_pid(pid: str) -> bool:
    return len(pid) == 9 and pid.isdigit()

VALIDATION = {
    'byr': _validate_byr,
    'iyr': _validate_iyr,
    'eyr': _validate_eyr,
    'hgt': _validate_hgt,
    'hcl': _validate_hcl,
    'ecl': _validate_ecl,
    'pid': _validate_pid,
    # 'cid': _validate_cid      # This field is supposed to be ignored
}

def _validate_passport_fields(passport: dict) -> bool:
    for field in VALIDATION:
        if field not in passport:
            return False

    return True

def _validate_passport_values(passport: dict) -> bool:
    for field in VALIDATION:
        if field not in passport or not VALIDATION[field](passport[field]):
            return False

    return True

def solve_part_1(munged_input: list):
    # return count of valid passports in input
    return len(list(filter(_validate_passport_fields, munged_input)))

def solve_part_2(munged_input: list):
    return len(list(filter(_validate_passport_values, munged_input)))

if __name__ == '__main__':
    import sys, os
    raw_input = get_input(4)
    munged_input = get_passports(raw_input)
    print(f'The solution to the first part is: {solve_part_1(munged_input)}.')
    print(f'The solution to the second part is: {solve_part_2(munged_input)}.')
