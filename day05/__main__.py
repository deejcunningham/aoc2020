from util import get_input

def _recurse_binary_space(instruction_set: list, space: list):
    # recursively eliminate slices of binary_space
    while instruction_set:
        binary = instruction_set.pop(0)
        if binary == 'L':
            remaining_space = space[:len(space)//2]
        elif binary == 'U':
            remaining_space = space[len(space)//2:]
        return _recurse_binary_space(instruction_set, remaining_space)

    return list(space)

def _munge_instructions(instruction_set: str):
    # map instructions ("F"/"B" and "L"/"R") to standard "L"/"U" ("lower"/"upper")
    return list(instruction_set.replace('F', 'L').replace('B', 'U').replace('R', 'U'))

def _calculate_seat_id(row, column) -> int:
    return row * 8 + column

def get_seat_id(instruction_set: str):
    row_instructions = _munge_instructions(instruction_set[:7])
    row = _recurse_binary_space(row_instructions, range(0, 128))[0]

    column_instructions = _munge_instructions(instruction_set[-3:])
    column = _recurse_binary_space(column_instructions, range(0, 8))[0]

    return _calculate_seat_id(row, column)

def solve_part_1(raw_input: list) -> int:
    # Return max seat ID
    return max([get_seat_id(inst) for inst in raw_input])

def solve_part_2(raw_input: list) -> int:
    # Return my seat
    # Get all possible seats
    theoretical_seats = [_calculate_seat_id(row, column) for column in range(0, 8) for row in range(0, 128)]
    # Get accounted for seats
    accounted_seats = [get_seat_id(inst) for inst in raw_input]
    missing_seats = set(theoretical_seats) - set(accounted_seats)
    # Find the missing seat where neighbors +/- 1 are accounted for
    return list(filter(lambda x: x + 1 in accounted_seats and x - 1 in accounted_seats, missing_seats))[0]

if __name__ == '__main__':
    import sys, os
    raw_input = get_input(5)
    print(f'The solution to the first part is: {solve_part_1(raw_input)}.')
    print(f'The solution to the second part is: {solve_part_2(raw_input)}.')
