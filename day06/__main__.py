from util import get_grouped_input

def solve_part_1(raw_input: list) -> int:
    return sum([len(set(''.join(group))) for group in raw_input])

def solve_part_2(raw_input: list) -> int:
    return sum([len(set.intersection(*map(set, group))) for group in raw_input])

if __name__ == '__main__':
    import sys, os
    raw_input = get_grouped_input(6)
    print(f'The solution to the first part is: {solve_part_1(raw_input)}.')
    print(f'The solution to the second part is: {solve_part_2(raw_input)}.')
