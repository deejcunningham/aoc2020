from util import get_input


if __name__ == '__main__':
    import sys, os
    raw_input = get_input(4)
    munged_input = munge_input(raw_input)
    print(f'The solution to the first part is: {solve_part_1(munged_input)}.')
    print(f'The solution to the second part is: {solve_part_2(munged_input)}.')
