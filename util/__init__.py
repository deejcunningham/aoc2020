import os

def _day_path(day_number: int) -> str:
    # TODO-DJ: get this base_dir dynamically
    base_dir = '/home/dj/projects/aoc2020'
    day_dir = f'day{day_number:02d}'
    return os.path.join(base_dir, day_dir)

def get_input(day_number: int) -> list:
    """Use this when input is individual lines."""
    with open(os.path.join(_day_path(day_number), 'input'), 'r') as input:
        return list([line.strip() for line in input])

def munge_input_chunk_to_dict(raw_chunk: list) -> dict:
    """When individual lines of input are 'key:value' pairs."""
    chunk = {}
    for item in raw_chunk:
        key, value = item.split(':')
        chunk[key] = value
    return chunk

def get_grouped_input(day_number: int, is_dict=False) -> list:
    """
        Use this when input is groups of lines. I.e. day04, day06...

        is_dict == True will return each group as a dict, where each line is "key:value"
    """
    raw_input = get_input(day_number)
    # slice off list chunk up to the newline element: ''
    input_groups = []
    while len(raw_input) > 0:
        try:
            stop = raw_input.index('')
        except ValueError:  # index() will throw this on the last chunk b/c no '' exists
            stop = len(raw_input)
        finally:
            next_chunk = []
            for i in raw_input[:stop]:
                next_chunk.extend(i.split())
            if is_dict:
                input_groups.append(munge_input_chunk_to_dict(next_chunk))
            else:
                input_groups.append(next_chunk)
            raw_input = raw_input[stop+1:]  # we've processed this chunk, so drop it

    return input_groups

